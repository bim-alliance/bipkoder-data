# BIPkoder data

Det här repot innehåller data för bipkoder.se.

## Datastruktur

BIPkoder består av 5 st värdelistor. För att underlätta förvaltningen av dessa listor så lagras varje värde som egen JSON-fil. Filerna hittar du under sin respektive värdelistmapp under `input/data`.
