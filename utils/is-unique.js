export function isUnique(arrToTest) {
    return arrToTest.length === new Set(arrToTest).size
}
